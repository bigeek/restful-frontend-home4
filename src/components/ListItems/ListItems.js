import React, { useState, useEffect } from "react";
import ListItem from "../ListItem/ListItem";
import AddForm from "../AddForm/AddForm";
import defaultImage from "./image.svg";

function ListItems(props) {
  const apiUrl = "https://jsonplaceholder.typicode.com/photos/";

  const [items, setItems] = useState([]);
  useEffect(() => {
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        setItems(data.slice(0, 6)); //lets limit to 6 items
      });
  }, []);

  function deleteItem(id) {
    setItems(items.filter(item => item.id !== id));
    fetch(apiUrl + id, { method: "DELETE" })
      .then(response => response.json())
      .then(data => {
        console.log("deleted id:" + id);
      });
  }

  function editItem(id) {
    fetch(apiUrl + id, { method: "PUT" })
      .then(response => response.json())
      .then(data => {
        console.log("edited id:" + id);
      });
  }

  const [postImage, setpostImage] = useState([defaultImage]);

  const [click, setClick] = useState([]);
  useEffect(() => {
    console.log(click);
  });

  function setClickk(click) {
    setClick(click);
    setpostImage(items[click].url);
  }

  // const [formItem, setFormItem] = useState([]);

  // useEffect((click) => {
  //   setFormItem(items.filter(item => item.id == click));
  //   debugger
  // });

  return (
    <div className="main">
      <AddForm user={items.id} />
      <img className="image" src={postImage} alt="default" />
      <div className="items">
        {items.map(item => (
          <ListItem
            id={item.id}
            title={item.title}
            deleteItem={deleteItem}
            editItem={editItem}
            setClk={setClickk}
          />
        ))}
      </div>
    </div>
  );
}

export default ListItems;
