import React from "react";
import styled from "styled-components";
import editIcon from "./edit.svg";
import deleteIcon from "./delete.svg";


const Item = styled.li`
  position: relative;
  border-bottom: 1px #d1d1d1 solid;
  border-right: 1px #d1d1d1 solid;
  border-left: 1px #d1d1d1 solid;
  background: rgba(23, 25, 50, 0.6);
  &:first-child {
    border-top: 1px #d1d1d1 solid;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }
  &:last-child {
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
  }
  padding: 19px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  animation: ___CSS_0___ 1s ease;
`;

const Text = styled.span`
  position: relative;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 19px;
  color: #ffffff;

  transition: position 0.8 ease-out;
`;

const DelButton = styled.button`
  padding: 5px;
  background-color: transparent;
  border: 0;
  appearance: none;
  position: absolute;
  right: 17px;
`;

const EditButton = styled.button`
  padding: 5px;
  background-color: transparent;
  border: 0;
  appearance: none;
  position: absolute;
  right: 47px;
`;

function ListItem(props) {

  const { id, title , deleteItem, setClk, editItem} = props;

  return (
    <Item className="item" onClick={ () => setClk(id)}>
      <Text>{title}</Text>
      <EditButton>
        <img src={editIcon} onClick={() => {editItem(id)}} alt="edit" />
      </EditButton>
      <DelButton
        aria-label="Delete"
        onClick={() => {
          deleteItem(id);
        }}
      >
        <img src={deleteIcon} alt="remove" />
      </DelButton>
    </Item>
  );
}

export default ListItem;
