import React, { useState } from "react";
import styled from "styled-components";
import plusIcon from "./plus.svg";

const AddButton = styled.button`
  background-color: transparent;
  border: 0;
  position: absolute;
  left: 160px;
  bottom: -31px;
`;

const Input = styled.input.attrs(props => ({
  type: "text"
}))`
  width: 100%;
  height: 48px;
  background: #ffffff;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.15);
  margin-bottom: 16px;
`;

function AddForm(props) {
  const apiUrl = "https://jsonplaceholder.typicode.com/photos/";
  const { albumid, id, title, url } = props;

  const [aid, setAid] = useState([]);
  const [uid, setUid] = useState([]);
  const [titlee, setTitle] = useState([]);
  const [urll, setUrl] = useState([]);

  return (
    <form
      className="form"
      onSubmit={event => {
        event.preventDefault();

        fetch(apiUrl, {
          method: "POST",
          headers: new Headers(),
          body: JSON.stringify({
            albumId: aid,
            id: uid,
            title: titlee,
            url: urll
          })
        })
          .then(response => response.json())
          .then(data => {
            console.log(data);
          });
        event.target.elements.id.value = "";
        event.target.elements.albumid.value = "";
        event.target.elements.title.value = "";
        event.target.elements.url.value = "";
        debugger;
      }}
    >
      <Input
        name="albumid"
        value={albumid}
        placeholder="Album ID"
        onChange={e => setAid(e.target.value)}
      />
      <Input
        name="id"
        value={id}
        placeholder="pic id"
        onChange={e => setUid(e.target.value)}
      />
      <Input
        name="title"
        value={title}
        placeholder="Title text"
        onChange={e => setTitle(e.target.value)}
      />
      <Input
        className="bigInput"
        name="url"
        value={url}
        placeholder="Picture Url"
        onChange={e => setUrl(e.target.value)}
      />
      <AddButton>
        <img src={plusIcon} alt="add" />
      </AddButton>
    </form>
  );
}

export default AddForm;
