import React from "react";
import "./App.css";

import ListItems from "./components/ListItems/ListItems";

const App = () => {
  return (
    <div className="App">
      <span className="top_title">Posts Crud</span>
      <div className="main">
        <ListItems />
      </div>
    </div>
  );
};

export default App;
